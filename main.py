#!/usr/bin/env python
import datetime
import time
import re
import smtplib
from email.mime.text import MIMEText
from email.header import Header

def send_email(message,title = None, reciever_list= None):
    '''
    send email to user from cisco noreply
    :param reciever: str
    :param message: str
    :return:
    '''
    if not reciever_list:
        reciever = 'yuwu3@cisco.com'
    else:
        reciever = ','.join(reciever_list)
    if not isinstance(reciever, str):
        raise Exception('reciever must be str')
    send_title = title or 'Please check the Email'

    sender = 'donotreply@cisco.com'

    message = MIMEText('<html>' +
                       '<head><title>{0}</title></head>'.format(send_title) +
                       '<body>'+message+
                       '</body>' +
                       '</html>', 'html')
    message['From'] = Header("TargetProcess")
    message['To'] = Header(reciever)
    subject = send_title
    message['Subject'] = Header(subject)

    try:
        smtpObj = smtplib.SMTP('outbound.cisco.com')
        smtpObj.sendmail(sender, reciever, message.as_string())
        print('Sending email to ' + str(reciever))
        print("success")
    except smtplib.SMTPException:
        print("Error: can't send email")


if __name__ == "__main__":
    message = "Jekins Test"
    title = "Jekins Test"
    send_email(message,title)